# Morehouse College Lecture: CI/CD

"Continuous Integration and Continuous Delivery"

## Resources

- [Lectures](https://morehouse-dcherry.gitlab.io/advanced-software-engineering/lectures/)

The slides on the lectures page provide the step-by-step instructions as exercises for this repository.
